import           Application                 (mainTlsLog, makeApplication)
import           Prelude                     (IO)
import           Settings                    (parseExtra)
import           Yesod.Default.Config        (fromArgs)

main :: IO ()
main = mainTlsLog (fromArgs parseExtra) makeApplication 

{-# OPTIONS_GHC -fno-warn-orphans #-}
module Application
    ( makeApplication
    , getApplicationDev
    , makeFoundation
    , mainTlsLog
    ) where

import Import
import Settings
import Yesod.Auth
import Yesod.Default.Config
import Yesod.Default.Main
import Yesod.Default.Handlers
import Network.Wai.Handler.WarpTLS (runTLS)
import Network.Wai.Handler.Warp (defaultSettings, setPort, setHost, setOnException)
import qualified Network.Wai.Handler.Warp as Warp (defaultShouldDisplayException)
import Network.Wai.Middleware.RequestLogger
    ( mkRequestLogger, outputFormat, OutputFormat (..), IPAddrSource (..), destination
    )
import qualified Network.Wai.Middleware.RequestLogger as RequestLogger
import qualified Database.Persist
import Database.Persist.Sql (runMigration)
import Network.HTTP.Client.Conduit (newManager)
import Control.Monad.Logger (runLoggingT, liftLoc)
import System.Log.FastLogger (newStdoutLoggerSet, defaultBufSize, toLogStr)
import Network.Wai.Logger (clockDateCacher)
import Yesod.Core.Types (loggerSet, Logger (Logger))
import System.Environment (lookupEnv)
import Language.Haskell.TH.Syntax (qLocation)
import qualified Data.Text as T (pack)

-- Import all relevant handler modules here.
-- Don't forget to add new modules to your cabal file!
import Handler.Home
import Handler.Events
import Handler.Orgs
import Handler.Info

-- This line actually creates our YesodDispatch instance. It is the second half
-- of the call to mkYesodData which occurs in Foundation.hs. Please see the
-- comments there for more details.
mkYesodDispatch "App" resourcesApp

-- This function allocates resources (such as a database connection pool),
-- performs initialization and creates a WAI application. This is also the
-- place to put your migrate statements to have automatic database
-- migrations handled by Yesod.
makeApplication :: AppConfig DefaultEnv Extra -> IO (Application, LogFunc)
makeApplication conf = do
    foundation <- makeFoundation conf

    -- Initialize the logging middleware
    logWare <- mkRequestLogger def
        { outputFormat =
            if development
                then Detailed True
                else Apache FromSocket
        , destination = RequestLogger.Logger $ loggerSet $ appLogger foundation
        }

    -- Create the WAI application and apply middlewares
    app <- toWaiAppPlain foundation
    let logFunc = messageLoggerSource foundation (appLogger foundation)
    return (logWare $ defaultMiddlewaresNoLogging app, logFunc)

-- | Loads up any necessary settings, creates your foundation datatype, and
-- performs some initialization.
makeFoundation :: AppConfig DefaultEnv Extra -> IO App
makeFoundation conf = do
    manager <- newManager
    s <- staticSite
    dbconf <- withYamlEnvironment "config/postgresql.yml" (appEnv conf)
              Database.Persist.loadConfig >>=
              Database.Persist.applyEnv
    p <- Database.Persist.createPoolConfig (dbconf :: Settings.PersistConf)

    loggerSet' <- newStdoutLoggerSet defaultBufSize
    (getter, _) <- clockDateCacher

    oAuthKeys <- getOAuthKeys "BNET_OAUTH"

    let logger = Yesod.Core.Types.Logger loggerSet' getter
        foundation = App
            { settings = conf
            , getStatic = s
            , connPool = p
            , httpManager = manager
            , persistConfig = dbconf
            , appLogger = logger
            , battleNetOAuthKeys = oAuthKeys
            }

    -- Perform database migration using our application's logging settings.
    runLoggingT
        (Database.Persist.runPool dbconf (runMigration migrateAll) p)
        (messageLoggerSource foundation logger)

    return foundation
  where
    getEnvVar var = ((T.pack . fromMaybe "") <$> lookupEnv var)

    -- XXX: for some reason this yesod/yaml refuses to lookup "_env" values
    getOAuthKeys prefix = Settings.OAuthKeys
        <$> getEnvVar (prefix <> "_CLIENT_ID")
        <*> getEnvVar (prefix <> "_CLIENT_SECRET")

-- for yesod devel
getApplicationDev :: IO (Int, Application)
getApplicationDev =
    defaultDevelApp loader (fmap fst . makeApplication)
  where
    loader = Yesod.Default.Config.loadConfig (configSettings Development)
        { csParseExtra = parseExtra
        }

-- run application w
mainTlsLog :: (Show env, Read env)
               => IO (AppConfig env Extra)
               -> (AppConfig env Extra -> IO (Application, LogFunc))
               -> IO ()
mainTlsLog load getApp = do
    config <- load
    case extraTls (appExtra config) of
        Nothing ->
            defaultMainLog (fromArgs parseExtra) makeApplication
        Just tlsSettings -> do
            (app, logFunc) <- getApp config
            runTLS tlsSettings
                ( setPort (appPort config)
                $ setHost (appHost config)
                $ setOnException (const $ \e -> when (shouldLog' e) $ logFunc
                    $(qLocation >>= liftLoc)
                    "yesod"
                    LevelError
                    (toLogStr $ "Exception from Warp: " ++ show e))
                $ defaultSettings
                ) app
  where
    shouldLog' = Warp.defaultShouldDisplayException

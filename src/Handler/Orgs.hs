module Handler.Orgs where

import Import
import Yesod.Auth

getOrgRegisterR :: Handler Html
getOrgRegisterR = defaultLayout $(widgetFile "orgs/register")

postOrgRegisterR :: Handler Html
postOrgRegisterR = do
    aid <- requireAuthId
    morg <- maybeOrg
    case morg of
        Just _ -> redirect HomeR

        Nothing -> do
            org <- runInputPost $ Org
                <$> pure aid
                <*> ireq textField "title"
                <*> ireq textField "url"
                <*> ireq textField "descr"

            runDB (insert org) >> redirect HomeR

getOrgDetailR :: Int -> Handler Html
getOrgDetailR oid = do
    org <- runDB $ get404 (keyFromInt oid)
    events <- runDB $ selectList [ EventOrgId ==. keyFromInt oid ] [ Desc EventDate ]

    maid <- maybeAuthId
    madmin <- maybeAdmin

    let canEdit = madmin == Just True
               || maid == Just (orgMasterId org)

    defaultLayout $ do
        setTitle . toHtml $ orgTitle org
        $(widgetFile "orgs/details")

handleOrgEditR :: Int -> Handler Html
handleOrgEditR oid = do
    let oKey = keyFromInt oid :: Key Org
    org <- runDB $ get404 oKey

    maid <- maybeAuthId
    madmin <- maybeAdmin

    let canEdit = madmin == Just True
               || maid == Just (orgMasterId org)

    unless canEdit $ redirect (OrgDetailR oid)

    method <- fmap requestMethod waiRequest
    case method of
        "GET" -> defaultLayout $(widgetFile "orgs/edit")

        "POST" -> do
            newOrg <- runInputPost $ Org
                <$> pure (orgMasterId org)
                <*> ireq textField "title"
                <*> ireq textField "url"
                <*> ireq textField "descr"
            runDB $ replace oKey newOrg
            redirect $ OrgDetailR oid

        _ -> badMethod

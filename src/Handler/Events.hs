module Handler.Events where

import Import
import Yesod.Auth
import qualified Database.Esqueleto as E
import           Database.Esqueleto ((^.))
import qualified Data.Text as T
import qualified Data.Text.Encoding as T
import           GHC.Real ((%))

getEventCreateR :: Handler Html
getEventCreateR = do
    _ <- requireOrg

    defaultLayout $(widgetFile "events/create")

postEventCreateR :: Handler Html
postEventCreateR = do
    Entity oid _ <- requireOrg
    userUA <- fmap (fromMaybe "") $ lookupHeader "User-Agent"
    userIP <- fmap (fromMaybe "") $ lookupHeader "X-Forwarded-For"

    event <- runInputPost $
        Event <$> pure oid
              <*> ireq textField "title"
              <*> ireq checkBoxField "world"
              <*> ireq textField "city"
              <*> ireq  dayField "date"
              <*> ireq timeField "time"
              <*> ireq textField "url"
              <*> (fromMaybe "" <$> iopt textField "challonge")
              <*> ireq textField "descr"
              <*> pure False
              <*> pure (T.decodeUtf8 userIP)
              <*> pure (T.decodeUtf8 userUA)

    newKey <- runDB (insertUnique event)
    case newKey of
        Nothing -> error "Мероприятие с таким названием уже существует."
        Just key -> redirect $ EventDetailR (keyToInt key)

getEventListR :: Handler Html
getEventListR = do
    maid <- maybeAuthId
    morg <- maybeOrg
    madmin <- maybeAdmin

    query <- fmap (fromMaybe "") (lookupGetParam "query")
    let noQuery = query == ""

    archive <- fmap isJust (lookupGetParam "archive")
    fsgc    <- fmap isJust (lookupGetParam "fsgc")

    today <- fmap (localDay . zonedTimeToLocalTime) (liftIO getZonedTime)
    let delta = (0 -) . diffDays today
        isHot event = delta (eventDate event) < 7
                   && delta (eventDate event) >= 0
        isCold event = delta (eventDate event) < 0
        isToday event = delta (eventDate event) == 0

    page <- fmap (maybe 1 (read . T.unpack)) (lookupGetParam "page")
    let lim = 20 :: Int
        off = max 0 $ lim * (page - 1)

    let queryFilter e archive' fsgc' = do
            case (archive', fsgc') of
                (_, True)  -> E.where_ (e ^. EventWorld E.==. E.val True)
                (True, _)  -> E.where_ (e ^. EventDate E.<. E.val today)
                (False, _) -> E.where_ (e ^. EventDate E.>=. E.val today)

            -- By title
            E.where_ $ (E.lower_ (e ^. EventCity) `E.like` E.lower_ (E.val query E.++. (E.%)))
                 E.||. (E.lower_ (e ^. EventTitle) `E.like` E.lower_ ((E.%) E.++. E.val query E.++. (E.%)))

            -- Staff preview
            case (morg, madmin) of
                (_, Just True) -> return ()

                (Just (Entity oid _), _) ->
                    E.where_ (e ^. EventVerified E.==. E.val True
                        E.||. e ^. EventOrgId E.==. E.val oid )

                _ -> E.where_ (e ^. EventVerified E.==. E.val True)

    events <- runDB $ E.select $ E.from $ \(e `E.InnerJoin` o) -> do
        E.on (e ^. EventOrgId E.==. o ^. OrgId)
        queryFilter e archive fsgc
        if archive || fsgc
            then E.orderBy [E.desc (e ^. EventDate)]
            else E.orderBy [E.asc  (e ^. EventDate)]
        E.limit (fromIntegral lim) >> E.offset (fromIntegral off)
        return (e, o)

    let count archive' fsgc' = do
            [E.Value (cnt :: Int)] <- runDB $ E.select $
                E.from $ \(e :: E.SqlExpr (Entity Event)) -> do
                    queryFilter e archive' fsgc'
                    return E.countRows
            return cnt

    eventCount  <- count archive fsgc
    futureCount <- count False False
    pastCount   <- count True False
    champCount  <- count False True

    let numPages = max 1 . ceiling $ eventCount % lim
        pages = [1 .. numPages] :: [Int]

    Just route <- getCurrentRoute
    render <- getUrlRenderParams
    params <- fmap reqGetParams getRequest
    let pageUrl :: Int -> Text
        pageUrl p = render route
                  $ ("page", T.pack $ show p)
                  : [ (k, v) | (k, v) <- params
                             , k /= "page" ]

    defaultLayout $ do
        setTitle "Поиск мероприятий Hearthstone Fireside Gatherings"
        $(widgetFile "events/list")

getEventDetailR :: Int -> Handler Html
getEventDetailR eid = do
    event <- runDB $ get404 (keyFromInt eid)
    Just (Entity oKey org) <- runDB $ selectFirst [ OrgId ==. eventOrgId event ] []

    madmin <- maybeAdmin
    morg <- maybeOrg
    let canEdit = madmin == Just True
               || fmap entityKey morg == Just oKey

    defaultLayout $ do
        setTitle . toHtml $ eventTitle event
        if madmin == Just True
            then
                toWidget [julius|jQuery(function(){
                    $("#verify").on('change', function(){
                        $("#verify").prop("disabled", true)
                        $.post("@{EventEditR eid}", {"verify": this.checked}).always(function(){
                            $("#verify").prop("disabled", false)
                        })
                    })
                });|]
            else return ()
        $(widgetFile "events/detail")

handleEventEditR :: Int -> Handler Html
handleEventEditR eid = do
    let eKey = keyFromInt eid
    event <- runDB $ get404 eKey
    Just (Entity oKey _) <- runDB $ selectFirst [ OrgId ==. eventOrgId event ] []

    madmin <- maybeAdmin
    morg <- maybeOrg
    let canEdit = madmin == Just True
               || fmap entityKey morg == Just oKey

    unless canEdit $ redirect (EventDetailR eid)

    method <- fmap requestMethod waiRequest
    verify <- lookupPostParam "verify"
    case (method, verify) of
        ("GET", _) -> defaultLayout $ do
            setTitle . toHtml $ eventTitle event
            $(widgetFile "events/edit")

        ("POST", Just v) -> do
            runDB $ update eKey [ EventVerified =. (v == "true") ]
            return "ok"

        ("POST", _) -> do
            newEvent <- runInputPost $
                Event <$> pure (eventOrgId event)
                      <*> ireq textField "title"
                      <*> ireq checkBoxField "world"
                      <*> ireq textField "city"
                      <*> ireq  dayField "date"
                      <*> ireq timeField "time"
                      <*> ireq textField "url"
                      <*> (fromMaybe "" <$> iopt textField "challonge")
                      <*> ireq textField "descr"
                      <*> pure False
                      <*> pure (eventUserIP event)
                      <*> pure (eventUserUA event)
            runDB $ replace eKey newEvent
            redirect $ EventDetailR eid

        _ -> return "huh?"

getEventDeleteR :: Handler Html
getEventDeleteR = error "Not yet implemented: getEventDeleteR"

{-
selectRelated a b = E.select . E.from $
    \(x `E.InnerJoin` y) -> do
        E.on (x ^. a E.==. y ^. b)
        return (x, y)

infixr 5 ~~
a ~~ b = (a, b)
-}

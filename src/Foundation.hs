module Foundation where

import Prelude
import Yesod
import Yesod.Static
import Yesod.Auth
import Yesod.Auth.BrowserId
import Yesod.Default.Config
import Yesod.Default.Util (addStaticContentExternal)
import Network.HTTP.Client.Conduit (Manager, HasHttpManager (getHttpManager))
import qualified Settings
import Settings.Development (development)
import qualified Database.Persist
import Database.Persist.Sql (SqlBackend)
import Settings (widgetFile, Extra (..), OAuthKeys(..))
import Model
import Text.Jasmine (minifym)
import Text.Hamlet (hamletFile)
import Yesod.Core.Types (Logger)
import Data.Typeable (Typeable)
import Settings.StaticFiles
import Data.Maybe (fromMaybe, isJust)
import Auth.BattleNetOAuth (oAuth2BattleNet)

-- | The site argument for your application. This can be a good place to
-- keep settings and values requiring initialization before your application
-- starts running, such as database connections. Every handler will have
-- access to the data present here.
data App = App
    { settings :: AppConfig DefaultEnv Extra
    , getStatic :: Static -- ^ Settings for static file serving.
    , connPool :: Database.Persist.PersistConfigPool Settings.PersistConf -- ^ Database connection pool.
    , httpManager :: Manager
    , persistConfig :: Settings.PersistConf
    , appLogger :: Logger
    , battleNetOAuthKeys :: Settings.OAuthKeys -- ^ Keys for battle.net authentication
    }

instance HasHttpManager App where
    getHttpManager = httpManager

-- Set up i18n messages. See the message folder.
mkMessage "App" "messages" "en"

-- This is where we define all of the routes in our application. For a full
-- explanation of the syntax, please see:
-- http://www.yesodweb.com/book/routing-and-handlers
--
-- Note that this is really half the story; in Application.hs, mkYesodDispatch
-- generates the rest of the code. Please see the linked documentation for an
-- explanation for this split.
mkYesodData "App" $(parseRoutesFile "config/routes")

type Form x = Html -> MForm (HandlerT App IO) (FormResult x, Widget)

-- Please see the documentation for the Yesod typeclass. There are a number
-- of settings which can be configured by overriding methods here.
instance Yesod App where
    approot = ApprootMaster $ appRoot . settings

    -- Store session data on the client in encrypted cookies,
    -- default session idle timeout is 120 minutes
    makeSessionBackend _ = fmap Just $ defaultClientSessionBackend
        120    -- timeout in minutes
        "config/client_session_key.aes"

    defaultLayout widget = do
        master <- getYesod
        mmsg <- getMessage

        ma <- maybeAuth
        let displayName = case ma of 
                               Just (Entity _ user) -> case (userBattleTag user) of 
                                                            Just battleTag -> battleTag
                                                            Nothing -> (userIdent user)
                               _ -> "UNKNOWN"

        morg <- maybeOrg
        madmin <- maybeAdmin

        let keyToInt key =
                case keyToValues key of
                    (PersistInt64 v):_ -> fromIntegral v
                    x -> error $ "keyToInt " ++ show x

        -- We break up the default layout into two components:
        -- default-layout is the contents of the body tag, and
        -- default-layout-wrapper is the entire page. Since the final
        -- value passed to hamletToRepHtml cannot be a widget, this allows
        -- you to use normal widget features in default-layout.

        pc <- widgetToPageContent $ do
            addStylesheetRemote "https://fonts.googleapis.com/css?family=Ubuntu&subset=latin,cyrillic"
            addStylesheetRemote "https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css"
            -- addStylesheetRemote "http://bootswatch.com/slate/bootstrap.min.css"

            addScriptRemote "https://code.jquery.com/jquery-1.11.0.min.js"
            addScriptRemote "https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"

            addScriptRemote "https://yastatic.net/share/share.js"

            {-
            $(combineStylesheets 'StaticR
                [ css_normalize_css
                , css_bootstrap_css
                ])
            -}
            $(widgetFile "default-layout")
        withUrlRenderer $(hamletFile "templates/default-layout-wrapper.hamlet")

    -- This is done to provide an optimization for serving static files from
    -- a separate domain. Please see the staticRoot setting in Settings.hs
    urlRenderOverride y (StaticR s) =
        Just $ uncurry (joinPath y (Settings.staticRoot $ settings y)) $ renderRoute s
    urlRenderOverride _ _ = Nothing

    -- The page to be redirected to when authentication is required.
    authRoute _ = Just $ AuthR LoginR

    -- Routes not requiring authenitcation.
    isAuthorized (AuthR _) _ = return Authorized
    isAuthorized FaviconR _ = return Authorized
    isAuthorized RobotsR _ = return Authorized
    -- Default to Authorized for now.
    isAuthorized _ _ = return Authorized

    -- This function creates static content files in the static folder
    -- and names them based on a hash of their content. This allows
    -- expiration dates to be set far in the future without worry of
    -- users receiving stale content.
    addStaticContent =
        addStaticContentExternal minifym genFileName Settings.staticDir (StaticR . flip StaticRoute [])
      where
        -- Generate a unique filename based on the content itself
        genFileName lbs
            | development = "autogen-" ++ base64md5 lbs
            | otherwise   = base64md5 lbs

    -- Place Javascript at bottom of the body tag so the rest of the page loads first
    jsLoader _ = BottomOfBody

    -- What messages should be logged. The following includes all messages when
    -- in development, and warnings and errors in production.
    shouldLog _ _source level =
        development || level == LevelWarn || level == LevelError

    makeLogger = return . appLogger

-- How to run database actions.
instance YesodPersist App where
    type YesodPersistBackend App = SqlBackend
    runDB = defaultRunDB persistConfig connPool
instance YesodPersistRunner App where
    getDBRunner = defaultGetDBRunner connPool

instance YesodAuth App where
    type AuthId App = UserId

    -- Where to send a user after successful login
    loginDest _ = HomeR
    -- Where to send a user after logout
    logoutDest _ = HomeR

    getAuthId creds = runDB $ do
        x <- getBy $ UniqueUser $ credsIdent creds
        case x of
            Just (Entity uid _) -> do repsert uid user
                                      return $ Just uid
            Nothing -> do Just <$> insert user
      where user = User
                    { userIdent = credsIdent creds
                    , userBattleTag = lookup "battletag" $ credsExtra creds
                    , userPassword = Nothing
                    }

    -- You can add other plugins like BrowserID, email or OAuth here
    authPlugins m = [ oAuth2BattleNet oAuthClient oAuthSecret "eu" loginWidget
                    ]
      where oAuthClient = clientId $ battleNetOAuthKeys m
            oAuthSecret = clientSecret $ battleNetOAuthKeys m
            loginWidget = [whamlet|<br><input type="image" src="@{StaticR img_battlenet_png}" width="200" />|]

    authLayout body = defaultLayout $(widgetFile "auth")

    authHttpManager = httpManager

instance YesodAuthPersist App

-- This instance is required to use forms. You can modify renderMessage to
-- achieve customized and internationalized form validation messages.
instance RenderMessage App FormMessage where
    renderMessage _ _ = defaultFormMessage

-- | Get the 'Extra' value, used to hold data from the settings.yml file.
getExtra :: Handler Extra
getExtra = fmap (appExtra . settings) getYesod

newtype CachedOrg = CachedOrg { unCachedOrg :: Maybe (Entity Org) } deriving (Typeable)

maybeOrg :: Handler (Maybe (Entity Org))
maybeOrg = fmap unCachedOrg . cached . fmap CachedOrg $ do
    maid <- maybeAuthId
    maybe (return Nothing) (\aid -> runDB $ selectFirst [OrgMasterId ==. aid] []) maid

requireOrg :: Handler (Entity Org)
requireOrg = do
    morg <- maybeOrg
    return $ fromMaybe (error "Для этого действия надо стать организатором.") morg

maybeAdmin :: Handler (Maybe Bool)
maybeAdmin = do
    admins <- fmap extraAdmins getExtra

    ma <- maybeAuth
    return $ case ma of
        Just (Entity _ user) -> Just $ userIdent user `elem` admins
        _                    -> Nothing

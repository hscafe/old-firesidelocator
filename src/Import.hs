module Import
    ( module Import
    ) where

import           Prelude              as Import hiding (head, init, last,
                                                 readFile, tail, writeFile)
import           Yesod                as Import hiding (Route (..), timeField)

import           Control.Applicative  as Import (pure, (<$>), (<*>))
import           Control.Monad        as Import
import           Data.Text            as Import (Text)

import           Foundation           as Import
import           Model                as Import
import           Settings             as Import
import           Settings.Development as Import
import           Settings.StaticFiles as Import

import           Data.Maybe           as Import
import           Data.Monoid          as Import
                                                 (Monoid (mappend, mempty, mconcat),
                                                 (<>))

import           Data.Default         as Import
import           Data.Time            as Import hiding (parseTime)
import           Network.HTTP.Types   as Import (methodGet, methodPost)
import           Network.Wai          as Import (Request(..))

import           CMark (commonmarkToHtml, optHardBreaks, optNormalize)
import           Text.HTML.SanitizeXSS (sanitizeBalance)
import qualified Data.Text as T

keyToInt :: PersistEntity record => Key record -> Int
keyToInt key =
    case keyToValues key of
        (PersistInt64 v):_ -> fromIntegral v
        x -> error $ "keyToInt " ++ show x


keyFromInt :: PersistEntity record => Int -> Key record
keyFromInt int =
    case keyFromValues [PersistInt64 $ fromIntegral int] of
        Left e -> error $ "keyFromInt: " ++ show e
        Right k -> k

markdown :: Text -> Html
markdown = preEscapedToMarkup
         . sanitizeBalance
         . commonmarkToHtml [ optHardBreaks, optNormalize ]
         . T.replace "\r\n" "\n"

timeField :: Field Handler TimeOfDay
timeField = timeFieldTypeTime

disqusThread :: Widget
disqusThread = $(widgetFile "disqus")
